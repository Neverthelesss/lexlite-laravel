<?php

namespace App\Http\Requests;

use App\Contribute;
use Illuminate\Foundation\Http\FormRequest;

class UpdateContributeRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('product_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'required',
            ],
        ];
    }
}
