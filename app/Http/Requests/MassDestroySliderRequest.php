<?php

namespace App\Http\Requests;
use Gate;
use App\Slider;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroySliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return abort_if(Gate::denies('slider_delete'), 403, '403 Forbidden') ?? true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:sliders,id',
        ];
    }
}
