<?php

namespace App\Http\Requests;
use Gate;
use App\Home;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyHomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return abort_if(Gate::denies('home_delete'), 403, '403 Forbidden') ?? true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:homes,id',
        ];
    }
}
