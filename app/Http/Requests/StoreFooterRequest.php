<?php

namespace App\Http\Requests;
use App\Footer;
use Illuminate\Foundation\Http\FormRequest;

class StoreFooterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('footer_create');
    }

    public function rules()
    {
        return [
            'content' => [
                'required',
            ],
        ];
    }
}
