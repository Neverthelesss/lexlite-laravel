<?php

namespace App\Http\Requests;
use App\Slider;

use Illuminate\Foundation\Http\FormRequest;

class StoreSliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Gate::allows('slider_create');
    }

    public function rules()
    {
        return [
            'name'     => [
                'required',
            ],
            'image'    => [
                'required',
            ],
            
        ];
    }
}
