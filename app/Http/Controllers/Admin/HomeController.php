<?php

namespace App\Http\Controllers\Admin;
use App\Media;
use App\SubArticle;

class HomeController
{
    public function index()
    {
        $media = Media::latest()->get();
        $subarticle = SubArticle::latest()->get();
        return view('home',compact('media','subarticle'));
    }
    public function search(Request $request)
    {
        $search = $request->get('search');
        $articles = SubArticle::all()->where('title','like','%' .$search. '%')->paginate(5);
        return view('home',['articles' => $articles]);
    }
}
