<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySliderRequest;
use App\Http\Requests\StoreSliderRequest;
use App\Http\Requests\UpdateSliderRequest;
use App\Slider;

class SliderController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('slider_access'), 403);

        $sliders = Slider::all();

        return view('admin.sliders.index', compact('sliders'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('slider_create'), 403);

        return view('admin.sliders.create');
    }

    public function store(StoreSliderRequest $request)
    {
        abort_unless(\Gate::allows('slider_create'), 403);

        $slider = Slider::create($request->all());

        return redirect()->route('admin.sliders.index');
    }

    public function edit(Slider $slider)
    {
        abort_unless(\Gate::allows('slider_edit'), 403);

        return view('admin.sliders.edit', compact('slider'));
    }

    public function update(UpdateSliderRequest $request, Slider $slider)
    {
        abort_unless(\Gate::allows('slider_edit'), 403);

        $slider->update($request->all());

        return redirect()->route('admin.sliders.index');
    }

    public function show(Slider $slider)
    {
        abort_unless(\Gate::allows('slider_show'), 403);

        return view('admin.sliders.show', compact('slider'));
    }

    public function destroy(Slider $slider)
    {
        abort_unless(\Gate::allows('slider_delete'), 403);

        $slider->delete();

        return back();
    }

    public function massDestroy(MassDestroySliderRequest $request)
    {
        Slider::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
