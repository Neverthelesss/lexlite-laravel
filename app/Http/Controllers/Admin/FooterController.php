<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyFooterRequest;
use App\Http\Requests\StoreFooterRequest;
use App\Http\Requests\UpdateFooterRequest;
use App\Footer;

class FooterController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('footer_access'), 403);

        $footers = Footer::all();

        return view('admin.footers.index', compact('footers'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('footer_create'), 403);

        return view('admin.footers.create');
    }

    public function store(StoreFooterRequest $request)
    {
        abort_unless(\Gate::allows('footer_create'), 403);

        $footer = Footer::create($request->all());

        return redirect()->route('admin.footers.index');
    }

    public function edit(Footer $footer)
    {
        abort_unless(\Gate::allows('footer_edit'), 403);

        return view('admin.footers.edit', compact('footer'));
    }

    public function update(UpdateFooterRequest $request, Footer $footer)
    {
        abort_unless(\Gate::allows('footer_edit'), 403);

        $footer->update($request->all());

        return redirect()->route('admin.footers.index');
    }

    public function show(Footer $footer)
    {
        abort_unless(\Gate::allows('footer_show'), 403);

        return view('admin.footers.show', compact('footer'));
    }

    public function destroy(Footer $footer)
    {
        abort_unless(\Gate::allows('footer_delete'), 403);

        $footer->delete();

        return back();
    }

    public function massDestroy(MassDestroyFooterRequest $request)
    {
        Footer::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }

}
