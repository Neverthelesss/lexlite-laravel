-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Bulan Mei 2020 pada 01.53
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iicd`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `articles`
--

INSERT INTO `articles` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'article_access', 'http://localhost:8000/storage/photos/1/image/oakao.jpg', '<p>loremp ipsum dolot sit ametloremp ipsum dolot sit ametloremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet loremp ipsum dolot sit amet&nbsp;</p>', '2020-05-01 03:55:44', '2020-05-01 03:55:44', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `events`
--

INSERT INTO `events` (`id`, `title`, `image`, `content`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Testing Event 1', 'http://localhost:8000/storage/photos/1/image/oakao.jpg', '<p>dsasdasdadas</p>', 'training regular', '2020-05-01 07:17:00', '2020-05-01 07:17:00', NULL),
(2, 'dasda', 'http://localhost:8000/storage/photos/1/image/oakao.jpg', '<p>adajskdajsdjaks</p>', 'corporate governance award', '2020-05-01 12:25:43', '2020-05-01 12:25:43', NULL),
(3, 'dasda', 'http://localhost:8000/storage/photos/1/image/oakao.jpg', '<p>adajskdajsdjaks</p>', 'corporate governance award', '2020-05-01 12:25:44', '2020-05-01 12:25:48', '2020-05-01 12:25:48'),
(4, 'sadaaaaa', 'http://localhost:8000/storage/photos/1/image/101.jpg', '<p>halooo</p>', 'board forum', '2020-05-01 12:28:33', '2020-05-01 12:28:33', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `footers`
--

CREATE TABLE `footers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `founders`
--

CREATE TABLE `founders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `founders`
--

INSERT INTO `founders` (`id`, `name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Reynaldy', 'http://localhost:8000/storage/photos/1/image/101.jpg', '2020-05-01 07:47:20', '2020-05-01 07:47:20', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `homes`
--

CREATE TABLE `homes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `homes`
--

INSERT INTO `homes` (`id`, `about`, `video`, `video_text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '<div class=\"container\">\r\n<div class=\"row mt-5\" style=\"text-align: center;\">\r\n<div class=\"col-sm-12\">\r\n<div class=\"text-center  p-4\">\r\n<h2>About Who We Are</h2>\r\n<p class=\"p-2\"><strong>IICD and you as Partners, Alumnae, should work together to GROW FAST to foster the implementation of Good Governance in Indonesia, and East Asia countries.</strong></p>\r\n<p>IICD aspires to be a strategic partner in establishing good corporate governance by promoting the ethical corporate behavior and improving skills, knowledge and ability of corporate directors and commissioners.</p>\r\n<p>We concentrate our activities on professional education for directors and commissioners; research on corporate governace, directorship, and performance; as well as advocacy in education and research.</p>\r\n<p>As part of the role of IICD in internalizing best practices of good corporate governance and directorship in Indonesia, IICD had conducted the first corporate governance scorecard research that involved 61 leading Indonesian public-listed companies in 2005, and the second and third corporate governance scorecard in 2007 and 2009 that involved 341 Indonesian listed companies financed by the grants from CIPE (Center for International Private Enterprise). The results of the study needs to be disseminated to the regulators as well as the private sectors since they provide feedbacks to regulators and companies on how to improve governance practices in Indonesia. Firms that perform outstanding governance practices also need to be appreciated for their efforts and performances.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'https://www.youtube.com/embed/ht-1aW70Y6w', 'IICD has also conducted ROSC (Research on The Obsetrvance of Standards and Codes), a joint World Bank and International Monetary Fund (IMF) initiative to strenghten member countries financial system by improving complance with internationally standard and codes.', '2020-04-30 15:09:57', '2020-04-30 15:24:21', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(67, '2020_04_27_090639_create_slides_table', 1),
(92, '2014_10_12_100000_create_password_resets_table', 2),
(93, '2019_04_15_191331679173_create_1555355612601_permissions_table', 2),
(94, '2019_04_15_191331731390_create_1555355612581_roles_table', 2),
(95, '2019_04_15_191331779537_create_1555355612782_users_table', 2),
(96, '2019_04_15_191332603432_create_1555355612603_permission_role_pivot_table', 2),
(97, '2019_04_15_191332791021_create_1555355612790_role_user_pivot_table', 2),
(98, '2019_04_15_191441675085_create_1555355681975_products_table', 2),
(99, '2019_08_19_000000_create_failed_jobs_table', 2),
(100, '2020_04_25_061819_create_articles_table', 2),
(101, '2020_04_25_061846_create_events_table', 2),
(102, '2020_04_25_061918_create_footers_table', 2),
(103, '2020_04_25_061934_create_founders_table', 2),
(104, '2020_04_25_061951_create_homes_table', 2),
(105, '2020_04_25_062010_create_news_table', 2),
(106, '2020_04_25_062020_create_pages_table', 2),
(107, '2020_04_25_062026_create_partners_table', 2),
(108, '2020_04_25_062036_create_programs_table', 2),
(109, '2020_04_25_062045_create_reports_table', 2),
(110, '2020_04_25_062103_create_researchs_table', 2),
(111, '2020_04_25_062113_create_schedules_table', 2),
(112, '2020_04_25_062123_create_scheduleregists_table', 2),
(113, '2020_04_25_062153_create_subscribes_table', 2),
(114, '2020_04_27_091116_create_sliders_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `news`
--

INSERT INTO `news` (`id`, `title`, `image`, `content`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Testing article', 'http://localhost:8000/storage/photos/1/image/101.jpg', '<p>loremp ipsum dolor sit ametloremp ipsum dolor sit amet loremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit ametloremp ipsum dolor sit amet &nbsp;</p>', 'Perbanas', '2020-05-01 01:18:25', '2020-05-01 01:40:15', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `content`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Governance Board', 'governance-board', '<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-sm-12\">\r\n<p class=\"justifyfull\">Indonesian Institute for Corporate Directorship (IICD) is a non profit organization founded by ten (10) highly reputable Indonesian Business Schools and preeminent individuals. Since its inception in 2000, IICD which envisioned to \"Internalizing Best Practices of Good Corporate Governance and Directorship\" has been actively working on Seminars, Trainings, Panel Discussions, Curriculum Design, Consulting and Research activities on GCG and Directorship. IICD with its alumnae more than 5,000 Senior Manager, Directors, and Commissioners, positions itself to support the decision makers, as the strategic partner in GCG implementation. IICD programs have been supported by The World Bank, IFC, GCGF, CIPE, ADB and OJK as part of the development of Good Corporate Governance implementation in Indonesia. IICD is the member of IDEA.Net (Institute of Directors East Asia Network), a preeminent organization with a vision of enhancing the implementation of Good Corporate Governance conducts in Asia having members from several Asia countries including Singapore, Malaysia, Philippines, Thailand, Korea, China, HongKong, and Taiwan.</p>\r\n<p class=\"justifyfull\"><strong>Vision</strong><br />Internalizing Best Practices of Good Corporate Governance and Directorship<br /><br /><strong>Mission</strong><br />&nbsp;- &nbsp;&nbsp;To internalize best practices of good corporate governance.<br />&nbsp;- &nbsp;&nbsp;To improve skills, knowledge, and ability to accelerate good corporate governance<br /><br /><strong>Our Board<br /></strong></p>\r\n<table class=\"table table-striped\" style=\"width: 686px;\" border=\"0\" width=\"500\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 248px;\">Chairman of Trustee Board</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\">I<a href=\"http://localhost/iicd/en/profile/facilitators.html#18\">r. Andi Ilham Said, M.S.O.M, Ph.D</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Trustee Board</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Airlangga Hartarto</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Member</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#8\">Prof. Dr. Sammy Kristamuljana</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#5\">Prof.Dr.Sidharta Utama,Ph.D., CFA</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Prof. Dr.Ir.Budi Widianarko, MSc</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#4\">Ir. Sukono Soebekti,MBA, Ph.D</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Ir. Markus Makdin Sinaga, MSc</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Dr.Nugroho Budi Satrio</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Sukamdani,MBA,BET</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Drs. Palti M.T Sitorus, MM</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Erry Riyana Hardjapamekas</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">James Simanjuntak,Ph.D</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Arief Daryanto</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Dr. Ir. Kuntoro Mangkusubroto</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Dr. Agustinus Prasetyantoko</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">&nbsp;</td>\r\n<td style=\"width: 413px;\">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Chairman of Supervisory Board</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#13\">Emmy Ruru, SH. LLM</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Supervisory of Board</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Dr. Gede Harja Wasistha, CMA</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Supervisory of Board</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Gunawan Tjokro</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">&nbsp;</td>\r\n<td style=\"width: 413px;\">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Chairman of Executive Board</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#9\">Sigit Pramono</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Vice Chairman Education</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Prof. Dr. Lindawati Gani, CMA</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Vice Chairman Research</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#6\">Lukas Setia Atmaja, Ph.D</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Vice Chairman Corporation</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Endang Hoyaranda</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Vice Chairman Consultation</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#10\">Ir. Dodi Prawira, MBA</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Secretary of Management Board</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#15\">Suhunan N. Situmorang, SH</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Reni Lestari Razaki, MM</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Treasurer of Management Board</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Nina Insania K. Permana</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">&nbsp;</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html#14\">Ir. Johan Dharmawan, MS., MBA., Mphil</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 248px;\">Executive Director</td>\r\n<td style=\"width: 17px;\">\r\n<div align=\"center\">:</div>\r\n</td>\r\n<td style=\"width: 413px;\"><a href=\"http://localhost/iicd/en/profile/facilitators.html\">Vita Diani Satiadhi, MM</a></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', 'profile', '2020-04-30 15:40:13', '2020-04-30 15:40:13', NULL),
(2, 'In House Training', 'in-house-training', '<h1 class=\"mt-5 mb-5\">In-House Training</h1>\r\n<p class=\"lead text-white\">&nbsp;</p>\r\n<p>Indonesian Institute for Corporate Directorship (IICD) is a non profit organization founded by ten (10) highly reputable Indonesian Business Schools and preeminent individuals. Since its inception, IICD which envisioned to \"Internalizing Best Practices of Good Corporate Governance and Directorship\" has been actively working on Seminars, Trainings, Panel Discussions, Curriculum Design, and research activities on GCG and Directorship. IICD with its alumnae more than 1,700 Senior Managers, Directors, and Commissioners positions itself to support the decision makers, as the strategic partner in GCG implementation. IICD programs have been supported by The World Bank, International Finance Corporation, Governance Corporate Governance Forum, Center for International Private Enterprise, Asian Development Bank as part of the development of Good Corporate Governance implementation in Indonesia. IICD is the member of&nbsp; IDEA.Net (Institute of Directors East Asia Network), a preeminent organization with a vision of enhancing the implementation of Good Corporate Governance conducts in Asia having members from several Asia countries including Singapore, Malaysia, Philippines, Thailand, Korea, China, HongKong, and Taiwan.<br /><br />IICD concentrates the activities on professional education/training for directors and commissioners:</p>\r\n<p>&nbsp;</p>\r\n<ol class=\"style1\">\r\n<li>Professional Director Program<br />3 days training which is internalizing professionalism in directorship designed to enhance the capacity of corporate leaders i Indonesian using standard curriculum benchmarked to International Institutes of Directors, such as Institutes of Directors (IoD) UK. Australian Institute of Company Director (AICD). Nasional Association of Corporate Director (NACD) USA &amp; Yale University, and Global Corporate Gorvenance Forum (IFC-GCGF).<br /><br />Course Contents:<br />-&nbsp;&nbsp; &nbsp;Director Skills and Competencies<br />-&nbsp;&nbsp; &nbsp;Modern Firms and Corporate Governance Problem<br />-&nbsp;&nbsp; &nbsp;The Practices of Directorship<br />-&nbsp;&nbsp; &nbsp;Challenges of Professionalism at the Board<br />-&nbsp;&nbsp; &nbsp;Board Performance Evaluation<br />-&nbsp;&nbsp; &nbsp;Strategic Business Direction for Corporate Performance<br />-&nbsp;&nbsp; &nbsp;Trade Practices : Issues for Directors<br />-&nbsp;&nbsp; &nbsp;Law Practices : Issues for Directors<br />-&nbsp;&nbsp; &nbsp;Finance for Directors<br />-&nbsp;&nbsp; &nbsp;Management Trends for Director and Commissioner<br />-&nbsp;&nbsp; &nbsp;Risk Management for Directors<br />-&nbsp;&nbsp; &nbsp;CEO forum and company sharing</li>\r\n<li>In-depth Directorship Program\r\n<ul>\r\n<li>Skill, Knowledge and Competwncies of Commissioners and Directors</li>\r\n<li>Transformasi organisasi menghadai VUCAD</li>\r\n<li>Business Acumen</li>\r\n<li>Fraud and Prevention</li>\r\n<li>International Business Law</li>\r\n<li>Directors Compensation</li>\r\n<li>Board Performance Evaluationand Enterprise Risk Management &ndash; An Overview of Directors &amp;&nbsp; Commissioner</li>\r\n<li>Corporate Investment Strategies for Value Creation</li>\r\n</ul>\r\n</li>\r\n<li>Corporate Governance Leadership Program<br />Two days training programs based on the CG Board Leadership Training Resources Kit developed by GCGF &ndash; IFC. It follows the OECD principles of CG and provides a comprehensive curriculum from the role of boards to the approaches that boards can use to comply to CG best practices.<br />\r\n<ul>\r\n<li>An Overview of Corporate Governance</li>\r\n<li>Professional Practices of Disclosure and Transparency</li>\r\n<li>The Role of Shareowners and Stakeholders within the Corporate Governance System</li>\r\n<li>The Business Case for Corporate Governance</li>\r\n<li>Board Role, Directors&rsquo; Duties and Liabilities</li>\r\n<li>Implementing Corporate Governance Change</li>\r\n</ul>\r\n</li>\r\n</ol>', 'program', '2020-04-30 16:05:00', '2020-04-30 16:05:00', NULL),
(3, 'Regular Training', 'regular-training', '<p>oll</p>', 'program', '2020-04-30 18:04:13', '2020-04-30 18:04:13', NULL),
(4, 'Consulting', 'consulting', '<h1 class=\"mt-5 mb-5\">Consulting</h1>\r\n<p>Assigning companies to have a better system of corporate governance referring to global standard.<br /><br /><strong>Indonesian Institute for Corporate Directorship (IICD)</strong>&nbsp;is a non profit organization founded by ten (10) highly reputable Indonesian Business Schools and preeminent individuals. Since its inception, IICD which envisioned to \"Internalizing Best Practices of Good Corporate Governance and Directorship\" has been actively working on Seminars, Trainings, Panel Discussions, Curriculum Design, and research activities on GCG and Directorship. IICD with its alumnae more than 1,700 Senior Managers, Directors, and Commissioners positions itself to support the decision makers, as the strategic partner in GCG implementation. IICD programs have been supported by The World Bank, IFC, GCGF, CIPE as part of the development of Good Corporate Governance implementation in Indonesia. IICD is the member of&nbsp;<strong>&nbsp;IDEA.Net (Institute of Directors East Asia Network)</strong>, a preeminent organization with a vision of enhancing the implementation of Good Corporate Governance conducts in Asia having members from several Asia countries including&nbsp;<strong>Singapore, Malaysia, Philippines, Thailand, Korea, China, HongKong, and Taiwan.</strong><br /><br /><strong>Our Services &ndash; Consulting</strong><br />-&nbsp;&nbsp; &nbsp;&nbsp; GCG Code<br />-&nbsp;&nbsp; &nbsp;&nbsp; Board Manual<br />-&nbsp;&nbsp; &nbsp;&nbsp; Code of Conduct<br />-&nbsp;&nbsp; &nbsp;&nbsp; Internal Audit Charter<br />-&nbsp;&nbsp; &nbsp;&nbsp; Audit Committe Charter<br />-&nbsp;&nbsp; &nbsp;&nbsp; Conflict of Interest<br />-&nbsp;&nbsp; &nbsp;&nbsp; Nomination and Remuneration<br />-&nbsp;&nbsp; &nbsp;&nbsp; Board Secretary Charter<br />-&nbsp;&nbsp; &nbsp;&nbsp; Annual Report&nbsp; Design</p>', 'program', '2020-05-01 07:25:30', '2020-05-01 07:27:43', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `partners`
--

INSERT INTO `partners` (`id`, `name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Multivercity', 'http://localhost:8000/storage/photos/1/image/imgbin-japanese-art-logo-wave-fame-is-wave-japan-wave-stencil-art-HzLasmUAazMMvie0ZjHwBsqC9.jpg', '2020-05-01 07:48:15', '2020-05-01 07:48:15', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(2, 'permission_create', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(3, 'permission_edit', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(4, 'permission_show', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(5, 'permission_delete', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(6, 'permission_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(7, 'role_create', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(8, 'role_edit', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(9, 'role_show', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(10, 'role_delete', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(11, 'role_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(12, 'user_create', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(13, 'user_edit', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(14, 'user_show', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(15, 'user_delete', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(16, 'user_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(17, 'product_create', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(18, 'product_edit', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(19, 'product_show', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(20, 'product_delete', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(21, 'product_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(22, 'article_access', '2020-04-29 15:21:09', '2020-04-29 15:21:09', NULL),
(23, 'article_create', '2020-04-29 15:21:13', '2020-04-29 15:21:13', NULL),
(24, 'article_delete', '2020-04-29 15:21:18', '2020-04-29 15:21:34', '2020-04-29 15:21:34'),
(25, 'article_delete', '2020-04-29 15:21:23', '2020-04-29 15:21:23', NULL),
(26, 'article_edit', '2020-04-29 15:21:29', '2020-04-29 15:21:29', NULL),
(27, 'article_show', '2020-04-29 15:23:14', '2020-04-29 15:23:14', NULL),
(28, 'event_access', '2020-04-29 15:23:20', '2020-04-29 15:23:20', NULL),
(29, 'event_create', '2020-04-29 15:23:24', '2020-04-29 15:23:24', NULL),
(30, 'event_delete', '2020-04-29 15:23:28', '2020-04-29 15:23:28', NULL),
(31, 'event_edit', '2020-04-29 15:23:33', '2020-04-29 15:23:33', NULL),
(32, 'event_show', '2020-04-29 15:23:39', '2020-04-29 15:23:39', NULL),
(33, 'footer_access', '2020-04-29 15:23:44', '2020-04-29 15:23:44', NULL),
(34, 'footer_create', '2020-04-29 15:23:50', '2020-04-29 15:23:50', NULL),
(35, 'footer_delete', '2020-04-29 15:23:56', '2020-04-29 15:23:56', NULL),
(36, 'footer_edit', '2020-04-29 15:24:01', '2020-04-29 15:24:01', NULL),
(37, 'footer_show', '2020-04-29 15:24:11', '2020-04-29 15:24:11', NULL),
(38, 'founder_access', '2020-04-29 15:24:18', '2020-04-29 15:24:18', NULL),
(39, 'founder_create', '2020-04-29 15:24:23', '2020-04-29 15:24:23', NULL),
(40, 'founder_delete', '2020-04-29 15:24:29', '2020-04-29 15:24:29', NULL),
(41, 'founder_delete', '2020-04-29 15:24:37', '2020-04-29 15:24:37', NULL),
(42, 'founder_edit', '2020-04-29 15:24:43', '2020-04-29 15:24:43', NULL),
(43, 'founder_show', '2020-04-29 15:24:49', '2020-04-29 15:24:49', NULL),
(44, 'homepage_access', '2020-04-29 15:24:56', '2020-04-29 15:24:56', NULL),
(45, 'homepage_create', '2020-04-29 15:25:02', '2020-04-29 15:25:02', NULL),
(46, 'homepage_delete', '2020-04-29 15:25:07', '2020-04-29 15:25:07', NULL),
(47, 'homepage_edit', '2020-04-29 15:25:12', '2020-04-29 15:25:12', NULL),
(48, 'homepage_show', '2020-04-29 15:25:27', '2020-04-29 15:25:27', NULL),
(49, 'news_access', '2020-04-29 15:25:33', '2020-04-29 15:25:33', NULL),
(50, 'news_create', '2020-04-29 15:25:38', '2020-04-29 15:25:38', NULL),
(51, 'news_delete', '2020-04-29 15:25:42', '2020-04-29 15:25:42', NULL),
(52, 'news_edit', '2020-04-29 15:25:47', '2020-04-29 15:25:47', NULL),
(53, 'news_show', '2020-04-29 15:25:54', '2020-04-29 15:25:54', NULL),
(54, 'page_access', '2020-04-29 15:26:07', '2020-04-29 15:26:07', NULL),
(55, 'page_create', '2020-04-29 15:26:11', '2020-04-29 15:26:11', NULL),
(56, 'page_delete', '2020-04-29 15:26:17', '2020-04-29 15:26:17', NULL),
(57, 'page_edit', '2020-04-29 15:26:21', '2020-04-29 15:26:21', NULL),
(58, 'page_show', '2020-04-29 15:26:26', '2020-04-29 15:26:26', NULL),
(59, 'partner_access', '2020-04-29 15:26:31', '2020-04-29 15:26:31', NULL),
(60, 'partner_create', '2020-04-29 15:26:36', '2020-04-29 15:26:36', NULL),
(61, 'partner_delete', '2020-04-29 15:26:40', '2020-04-29 15:26:40', NULL),
(62, 'partner_edit', '2020-04-29 15:26:46', '2020-04-29 15:26:46', NULL),
(63, 'partner_show', '2020-04-29 15:26:52', '2020-04-29 15:26:52', NULL),
(64, 'program_access', '2020-04-29 15:26:56', '2020-04-29 15:26:56', NULL),
(65, 'program_create', '2020-04-29 15:27:02', '2020-04-29 15:27:02', NULL),
(66, 'program_delete', '2020-04-29 15:27:09', '2020-04-29 15:27:09', NULL),
(67, 'program_show', '2020-04-29 15:27:14', '2020-04-29 15:27:14', NULL),
(68, 'program_edit', '2020-04-29 15:27:25', '2020-04-29 15:27:25', NULL),
(69, 'report_access', '2020-04-29 15:27:52', '2020-04-29 15:27:52', NULL),
(70, 'report_create', '2020-04-29 15:27:56', '2020-04-29 15:27:56', NULL),
(71, 'report_delete', '2020-04-29 15:28:01', '2020-04-29 15:28:01', NULL),
(72, 'report_edit', '2020-04-29 15:28:05', '2020-04-29 15:28:05', NULL),
(73, 'report_show', '2020-04-29 15:28:10', '2020-04-29 15:28:10', NULL),
(74, 'research_access', '2020-04-29 15:28:18', '2020-04-29 15:28:18', NULL),
(75, 'research_create', '2020-04-29 15:28:23', '2020-04-29 15:28:23', NULL),
(76, 'research_delete', '2020-04-29 15:28:28', '2020-04-29 15:28:28', NULL),
(77, 'research_edit', '2020-04-29 15:28:33', '2020-04-29 15:28:33', NULL),
(78, 'research_show', '2020-04-29 15:28:38', '2020-04-29 15:28:38', NULL),
(79, 'schedule_access', '2020-04-29 15:28:56', '2020-04-29 15:28:56', NULL),
(80, 'schedule_create', '2020-04-29 15:29:00', '2020-04-29 15:29:00', NULL),
(81, 'schedule_delete', '2020-04-29 15:29:05', '2020-04-29 15:29:05', NULL),
(82, 'schedule_edit', '2020-04-29 15:29:13', '2020-04-29 15:29:13', NULL),
(83, 'schedule_show', '2020-04-29 15:29:21', '2020-04-29 15:29:21', NULL),
(84, 'scheduleregist_access', '2020-04-29 15:29:33', '2020-04-29 15:29:33', NULL),
(85, 'scheduleregist_create', '2020-04-29 15:29:39', '2020-04-29 15:29:39', NULL),
(86, 'scheduleregist_delete', '2020-04-29 15:29:51', '2020-04-29 15:29:51', NULL),
(87, 'scheduleregist_edit', '2020-04-29 15:30:03', '2020-04-29 15:30:03', NULL),
(88, 'scheduleregist_show', '2020-04-29 15:30:12', '2020-04-29 15:30:12', NULL),
(89, 'slider_access', '2020-04-29 15:30:31', '2020-04-29 15:30:31', NULL),
(90, 'slider_create', '2020-04-29 15:30:36', '2020-04-29 15:30:36', NULL),
(91, 'slider_delete', '2020-04-29 15:30:41', '2020-04-29 15:30:41', NULL),
(92, 'slider_delete', '2020-04-29 15:30:46', '2020-04-29 15:30:51', '2020-04-29 15:30:51'),
(93, 'slider_edit', '2020-04-29 15:30:59', '2020-04-29 15:30:59', NULL),
(94, 'slider_show', '2020-04-29 15:31:07', '2020-04-29 15:31:07', NULL),
(95, 'subscribe_access', '2020-04-29 15:31:11', '2020-04-29 15:31:11', NULL),
(96, 'subscribe_create', '2020-04-29 15:31:17', '2020-04-29 15:31:17', NULL),
(97, 'subscribe_delete', '2020-04-29 15:31:22', '2020-04-29 15:31:22', NULL),
(98, 'subscribe_edit', '2020-04-29 15:31:27', '2020-04-29 15:31:27', NULL),
(99, 'subscribe_show', '2020-04-29 15:31:33', '2020-04-29 15:31:33', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(1, 22),
(1, 23),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 72),
(1, 73),
(1, 74),
(1, 75),
(1, 76),
(1, 77),
(1, 78),
(1, 79),
(1, 80),
(1, 81),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 93),
(1, 94),
(1, 95),
(1, 96),
(1, 97),
(1, 98),
(1, 99);

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `programs`
--

CREATE TABLE `programs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `programs`
--

INSERT INTO `programs` (`id`, `title`, `attachment`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PDP', 'http://localhost:8000/storage/files/1/Soal Forum_Travelling Salesperson Problem dan Rute Terpendek.pdf', '<p>dsada</p>', '2020-04-30 18:29:37', '2020-04-30 18:29:37', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `reports`
--

INSERT INTO `reports` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Report 2020', 'http://localhost:8000/storage/photos/1/image/103.jpg', '<p>dsadas</p>', '2020-05-01 16:18:20', '2020-05-01 16:18:20', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `researches`
--

CREATE TABLE `researches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `researches`
--

INSERT INTO `researches` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'sada', 'http://localhost:8000/storage/photos/1/image/96.jpg', '<p>asdasdas</p>', '2020-05-01 12:09:41', '2020-05-01 12:09:41', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', '2019-04-15 12:13:32', '2019-04-15 12:13:32', NULL),
(2, 'User', '2019-04-15 12:13:32', '2019-04-15 12:13:32', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `schedules`
--

CREATE TABLE `schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `schedules`
--

INSERT INTO `schedules` (`id`, `name`, `image`, `year`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Heloworld', 'http://localhost:8000/storage/photos/1/image/vlcsnap-2020-03-24-10h09m09s123.png', '2020', '<p>halodsada</p>', '2020-04-29 15:40:10', '2020-04-29 15:46:56', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `schedule_regists`
--

CREATE TABLE `schedule_regists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `schedule_regists`
--

INSERT INTO `schedule_regists` (`id`, `title`, `image`, `attachment`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Download asada', 'http://localhost:8000/storage/photos/1/image/103.jpg', 'http://localhost:8000/storage/files/1/Soal Forum_Travelling Salesperson Problem dan Rute Terpendek.pdf', '2020-05-01 16:32:15', '2020-05-01 16:38:54', NULL),
(3, 'aaaa', 'http://localhost:8000/storage/photos/1/image/96.jpg', 'http://localhost:8000/storage/files/1/Soal Forum_Travelling Salesperson Problem dan Rute Terpendek.pdf', '2020-05-01 16:42:47', '2020-05-01 16:42:47', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sliders`
--

INSERT INTO `sliders` (`id`, `name`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Helo', 'http://localhost:8000/storage/photos/1/image/101.jpg', '2020-04-30 15:13:57', '2020-04-30 15:13:57', NULL),
(2, 'Helo 2', 'http://localhost:8000/storage/photos/1/image/103.jpg', '2020-04-30 15:14:11', '2020-04-30 15:14:11', NULL),
(3, 'Heloaaa', 'http://localhost:8000/storage/photos/1/image/96.jpg', '2020-04-30 15:14:22', '2020-04-30 15:14:22', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subscribes`
--

CREATE TABLE `subscribes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `subscribes`
--

INSERT INTO `subscribes` (`id`, `name`, `company`, `email`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Heloaaa', '1231', 'admin@admin.com', '2020-04-30 15:35:58', '2020-04-30 15:35:58', NULL),
(2, 'Helo', 'sadaaaa', 'admin@admin.com', '2020-04-30 15:36:14', '2020-04-30 15:36:14', NULL),
(3, 'Erick Dazki', 'Rerere', 'admin@admin.com', '2020-05-01 16:50:20', '2020-05-01 16:50:20', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$imU.Hdz7VauIT3LIMCMbsOXvaaTQg6luVqkhfkBcsUd.SJW2XSRKO', 'Bobeq5tZGmQ63wF5alqhQwhZCYi7BtN47c206WV1GaFkwYVMXQMxjXKzolXg', '2019-04-15 12:13:32', '2019-04-15 12:13:32', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `footers`
--
ALTER TABLE `footers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `founders`
--
ALTER TABLE `founders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `permission_role_role_id_foreign` (`role_id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `researches`
--
ALTER TABLE `researches`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `schedule_regists`
--
ALTER TABLE `schedule_regists`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `footers`
--
ALTER TABLE `footers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `founders`
--
ALTER TABLE `founders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `homes`
--
ALTER TABLE `homes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT untuk tabel `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `programs`
--
ALTER TABLE `programs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `researches`
--
ALTER TABLE `researches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `schedule_regists`
--
ALTER TABLE `schedule_regists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ketidakleluasaan untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
