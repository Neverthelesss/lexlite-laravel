<?php

// Route::redirect('/', '/login');

Route::redirect('/home', '/admin');
Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/profile/{slug}', 'WelcomeController@showpage');
Route::get('/about', 'WelcomeController@showabout');
Route::get('/member', 'WelcomeController@showmember');
Route::get('/result/{research}', 'WelcomeController@showdetailresearch');
Route::get('/article/{article}', 'WelcomeController@showdetailarticle');
// Route buat contoh article
Route::get('/article', 'WelcomeController@showarticle')->name('edusection.index');
Route::get('/article/subarticle/{id}', 'WelcomeController@showsubarticle')->name('subarticle.ind');
Route::get('/article/subarticle/detail/{subarticle}', 'WelcomeController@showdetail')->name('detailsub.show');
// Route buat supportus
Route::get('/support', 'WelcomeController@support');

Auth::routes(['register' => false]);
Route::get('logout', function ()
{
    auth()->logout();
    Session()->flush();

    return Redirect::to('/');
})->name('logout');

Route::get('/search','WelcomeController@search');
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');


    Route::resource('roles', 'RolesController');
;

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    Route::delete('contributes/destroy', 'ContributeController@massDestroy')->name('contributes.massDestroy');

    Route::resource('contributes', 'ContributeController');

    Route::delete('requests/destroy', 'RequestsController@massDestroy')->name('requests.massDestroy');

    Route::resource('requests', 'RequestsController');

    // Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    // Route::resource('products', 'ProductsController');

    // Route::resource('articles', 'ArticleController');

    // Route::delete('articles/destroy', 'ArticleController@massDestroy')->name('articles.massDestroy');


    // Route::resource('subarticles', 'SubArticleController');
    Route::get('subarticles/creates/{id}', 'SubArticleController@creates')->name('subarticles.creates');
    Route::get('subarticles/show/{subarticle}/detail', 'SubArticleController@show')->name('subarticles.shows');
    Route::get('subarticles/{subarticle}/edit', 'SubArticleController@edit')->name('subarticles.edit');
    Route::post('subarticles/store', 'SubArticleController@store')->name('subarticles.store');
    Route::get('subarticles/detail/{id}', 'SubArticleController@index')->name('subarticles.indexs');
    Route::post('subarticles/update', 'SubArticleController@update')->name('subarticles.update');
    Route::get('subarticles/{id}', 'SubArticleController@index')->name('subarticles.destroy');
    Route::delete('subarticles/destroy', 'SubArticleController@massDestroy')->name('subarticles.massDestroy');
    Route::resource('subarticles', 'SubArticleController');



    Route::resource('medias', 'MediaController');
    Route::delete('medias/destroy', 'MediaController@massDestroy')->name('medias.massDestroy');

    Route::resource('footers', 'FooterController');
    Route::delete('footers/destroy', 'FooterController@massDestroy')->name('footers.massDestroy');

    Route::resource('homes', 'HomePageController');
    Route::delete('homes/destroy', 'HomePageController@massDestroy')->name('homes.massDestroy');


    Route::resource('sliders', 'SliderController');
    Route::delete('sliders/destroy', 'SliderController@massDestroy')->name('sliders.massDestroy');

    Route::group(['prefix' => 'laravel-filemanager','middleware' => ['auth']], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();});


});
Route::group(['prefix' => 'laravel-filemanager'], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();});



