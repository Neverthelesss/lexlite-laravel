@extends('partials.header')
@extends('partials.nav')
<div class="container pt-5 mt-5">
    <div class="mt-5 pt-5 mb-5 pb-5">
            <h1>{{ $program->title }}</h1>
            <span><b> {{ \Carbon\Carbon::parse($program->created_at)->format('D, d F Y') }} </b></span><br>
            {!! $program->content !!}
            <br>
            <a href="{{ $program->attachment }}" download>Download Attachment This Program</a>
    </div>
</div>
@extends('partials.footer')