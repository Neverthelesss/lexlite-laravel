@extends('partials.layout')

@section('content')
<div id="support-us" class="prim-color">
    <div class="container text-center py-5">
			<h1 class="display-4 white-text mt-3 acc-color-text">Support Us</h1>
			<div class="row justify-content-center">
				<div class="img-sample my-2"></div>
			</div>
			<div class="row justify-content-center">
				<p class="desc-text">Help us grow, produce more videos and continuously improve our quality. You can support us with a single or monthly donation. See the benefit below</p>
			</div>
			<a href="#" class="btn-hover">Become a Patron</a>
    </div>
</div>

<div id="benefit" class="acc-color">
	<div class="container py-5 text-center">
		<h1 class="display-4 white-text my-3 ">Benefits</h1>
		<div class="row">
			<div class="col-md-4">
				<div class="container">
					<i class="fas fa-box fa-4x white-text mb-2"></i>
					<div class="row justify-content-center">
						<div class="divider-white"></div>
					</div>					
					<p class="benefit-desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint omnis repellendus impedit nemo nesciunt ipsum consequuntur eveniet inventore iure</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="container">
					<i class="fas fa-box fa-4x white-text mb-2"></i>
					<div class="row justify-content-center">
						<div class="divider-white"></div>
					</div>					
					<p class="benefit-desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint omnis repellendus impedit nemo nesciunt ipsum consequuntur eveniet inventore iure</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="container">
					<i class="fas fa-box fa-4x white-text mb-2"></i>
					<div class="row justify-content-center">
						<div class="divider-white"></div>
					</div>					
					<p class="benefit-desc">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint omnis repellendus impedit nemo nesciunt ipsum consequuntur eveniet inventore iure</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection