@extends('partials.header')
@extends('partials.nav')
<div class="container pt-5 mt-5">
    <div class="mt-5 pt-5 mb-5 pb-5">
            <h1>{{ $report->title }}</h1>
            <span><b> {{ \Carbon\Carbon::parse($report->created_at)->format('D, d F Y') }} </b></span><br>
            
            <img class="img-fluid  pt-3 pb-3" style="height:350px;object-fit: cover;    " src="{{ $report->image }}" alt="">
            {!! $report->content !!}
            <br>
    </div>
</div>
@extends('partials.footer')