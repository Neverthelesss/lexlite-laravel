@extends('partials.header')
@extends('partials.nav')
<div class="mt-3 mb-4">
        <h1 class="display-2 text-white mt-5 text-center">All New Article
            </h1>
        </div>
    <!-- end of header -->
<div class="container">
<div class="row">
    @foreach ($subarticles as $subarticle)
    <div class="col-xl-6 col-sm-12">
        <div class=" bg-faded">
            <div class="card-body">
              <div class="row">
        <div class="col-4">
            <img src="img/6.jpg" alt="" class="rounded img-fluid" src="{{ $subarticle->image }}" style=" height: 250px;object-fit: cover">
        </div>
        <!-- start card content -->
        <div class="col-8">
            <h5>
                <a class="text-bluealter" href="{{ $subarticle->id }}">{{ $subarticle->title }}</a>
                <small><i class="far fa-clock"></i> {{ \Carbon\Carbon::parse($subarticle->created_at)->format('d F, Y') }}</small>
                <p>{!! substr($subarticle->content,0,200) !!}....</p>
                <div class="btn-group">
                    <p>
                    <a class="text-danger" href="{{ $subarticle->id }}">Detail news</a>
                    </p>
                </div>
            </div>

        </div>
  </div>
</div>
</div>
            <!-- end card -->
            @endforeach

      </div>

</div>
</div>
@extends('partials.footer')
