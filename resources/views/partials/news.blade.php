@extends('partials.header')
@extends('partials.nav')
<div class="jumbotron paral paralsec mt-5">
        <h1 class="display-2 text-white mt-5 text-center">News
            </h1>
        </div>
    <!-- end of header -->
<div class="container">
<div class="row">
      <div class="col-12">
              <div class="row">
                      <div class="container">
                              <div class="row">
                                  <div class="col-12">
                                      <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                          <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Perbanas</a>
                                          <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Collaboration</a>
                                        </div>
                                      </nav>
                                      <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                            <!--  row list evet -->
                                                
                                          <div class="row" id="image">
                                                @foreach ($perbanas as $perbanasitem)
                                            <div class="col-sm-12">
                                                <div class="card w-100 flex-md-row mb-4 shadow-sm h-md-250">
                                                        <div class="card-body w-50 d-flex flex-column align-items-start">
                                                            <h6 class="mb-0">
                                                                <a class="text-dark" href="#">{{ $perbanasitem->title }}</a>
                                                            </h6>
                                                            <div class="mb-1 text-muted small">{{ \Carbon\Carbon::parse($perbanasitem->created_at)->format('d F, Y') }}</div>
                                                            <p class="card-text mb-auto">{!! substr($perbanasitem->content,0,200) !!}....</p>
                                                            <a class="btn btn-outline-success btn-sm" href="/news/{{ $perbanasitem->id }}">Detail News</a>
                                                        </div>
                                                        <img class="card-img-right flex-auto d-none d-lg-block w-50" alt="Thumbnail [200x250]" src="{{ $perbanasitem->image }}" style=" height: 250px;object-fit: cover;">
                                                    </div>
                                           </div>
                                           @endforeach
                                      
                                            </div>
                                            <!-- end list event -->
                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                          <!--  row list evet -->
                                          <div class="row" id="image">
                                                @foreach ($collaboration as $collabitem)
                                                @if ($collabitem = null)
                                                    <p>Oops There is No Latest Data News</p>
                                                @else
                                                    
                                                <div class="col-sm-12">
                                                    <div class="card w-100 flex-md-row mb-4 shadow-sm h-md-250">
                                                            <div class="card-body w-50 d-flex flex-column align-items-start">
                                                                <h6 class="mb-0">
                                                                    <a class="text-dark" href="#">{{ $collabitem->title }}</a>
                                                                </h6>
                                                                <div class="mb-1 text-muted small">{{ \Carbon\Carbon::parse($collabitem->created_at)->format('d F, Y') }}</div>
                                                                <p class="card-text mb-auto">{!! substr($perbanasitem->content,0,200) !!}....</p>
                                                                <a class="btn btn-outline-success btn-sm" href="/news/{{ $collabitem->id }}">Detail News</a>
                                                            </div>
                                                            <img class="card-img-right flex-auto d-none d-lg-block w-50" alt="Thumbnail [200x250]" src="{{ $collabitem->image }}" style=" height: 250px;object-fit: cover;">
                                                </div>
                                               </div>
                                               @endif

                                               @endforeach
                                            
                                          </div>
                                          <!-- end list event -->  
                                        </div>
                                        
                                      </div>
                                      
                                  </div>
                              </div>
                          </div>
                 
                  </div>     <!-- end of list seminars and collabs  -->
      </div>
  </div>
</div>
@extends('partials.footer')