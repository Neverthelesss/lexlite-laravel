@extends('partials.header')
@extends('partials.nav')

<div class="jumbotron paral paralsec mt-5">
        <h1 class="display-2 text-white mt-5 text-center">All Event
            </h1>
        </div>
    <!-- end of header -->
<div class="container">
<div class="row">
    <div class="col-12">
        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">training</a>
            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">seminar</a>
          </div>
        </nav>
        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <!--  row list evet -->
            <div class="row" id="image">
                @foreach ($trainings as $training)
                <div class="col-sm-12">
                    <div class="col-xl-6 col-sm-12">
                        <div class=" bg-faded">
                            <div class="card-body">
                              <div class="row">
                        <div class="col-4">
                          <img src="{{ asset('img/6.jpg') }}" alt="" class="rounded img-fluid" />
                                </div>
                                 <div class="col-8">
                                        <h5>
                                            <a class="text-bluealter" href="/event/detail/{{ $training->id }}">{{ $training->title }}</a>  </h5>
                                            <small><i class="far fa-clock"></i> 07/07/2020</small>
                                        <p>
                                            {!! substr($training->content,0,110) !!}
                                        </p>
                                        <!-- Split button -->
                                       <div class="btn-group">

                                            <p>[ <a href="/event/detail/{{ $training->id }}" class="text-danger">More</a> ]</p>
                                        </div>
                  <!-- End Button -->
                                    </div>
                                    <!-- End columns 7 -->
                              </div>
                            </div>
                          </div>
                    </div>
                    </div>
                @endforeach
              </div>
              <!-- end list event -->
          </div>

          <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
            <!--  row list evet -->
            <div class="row" id="image">
                    @foreach ($seminars as $seminar)
                    <div class="col-sm-12">
                        <div class="col-xl-6 col-sm-12">
                            <div class=" bg-faded">
                                <div class="card-body">
                                  <div class="row">
                            <div class="col-4">
                              <img src="{{ asset('img/6.jpg') }}" alt="" class="rounded img-fluid" />
                                    </div>
                                     <div class="col-8">
                                            <h5>
                                                <a class="text-bluealter" href="/event/detail/{{ $seminar->id }}">{{ $seminar->title }}</a>  </h5>
                                                <small><i class="far fa-clock"></i> 07/07/2020</small>
                                            <p>
                                                {!! substr($seminar->content,0,110) !!}
                                            </p>
                                            <!-- Split button -->
                                           <div class="btn-group">

                                                <p>[ <a href="/event/detail/{{ $seminar->id }}" class="text-danger">More</a> ]</p>
                                            </div>
                      <!-- End Button -->
                                        </div>
                                        <!-- End columns 7 -->
                                  </div>
                                </div>
                              </div>
                        </div>
                        </div>
                    @endforeach
            </div>
            <!-- end list event -->
          </div>
        </div>

        <!-- MOdal Image -->
        {{-- <div class="modal fade " id="imagemodal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <img class="modal-img img-fluid" />
                </div>
            </div>
        </div> --}}
        <!-- end of modal image -->
    </div>
</div>
</div>
@extends('partials.footer')
