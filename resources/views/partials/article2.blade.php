@extends('partials.layout')

@section('content')

<div id="article" class="prim-color py-5">
  <h1 class="display-3 text-center acc-color-text mb-3">All Article</h1>
  <div class="container">
    <div class="row">
      @foreach ($Subarticles as $sub)
      <div class="col-md-4 col-sm-12 mb-3">
        <div class="card shadow">
          <img src="{{ $sub->image }}" class="card-img-top img-fluid" style=" height: 250px;object-fit: cover;" alt="Post Image">
          <div class="card-body prim-color">
            <h5 class="card-title">
              <a href="{{  route('detailsub.show', $sub->id)}}" class="text-white">{{ $sub->title }}</a>
            </h5>

            <a href="{{  route('detailsub.show', $sub->id)}}" class="btn acc-color-text white">See Article</a>
            <small class="grey-text float-right"><i class="fas fa-calendar-alt"></i> {{ \Carbon\Carbon::parse($sub->created_at)->format('d F Y') }}</small>

          </div>
        </div>
      </div>
      @endforeach

    </div>
  </div>
</div>

@endsection
