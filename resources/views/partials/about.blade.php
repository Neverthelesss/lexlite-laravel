@extends('partials.layout')

@section('content')
	<div id="about-us-1" class="prim-color">
		<div class="container py-5">
			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-about-us" role="tab" aria-controls="pills-home" aria-selected="true">About Us</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-our-mission" role="tab" aria-controls="pills-profile" aria-selected="false">Our Mission</a>
				</li>
			</ul>
			<div class="tab-content" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-about-us" role="tabpanel" aria-labelledby="pills-home-tab">
					<div class="row">
						<div class="col-md-4 pt-3">
							<div class="logo"></div>
						</div>
						<div class="col-md-8 pt-3 desc-text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="pills-our-mission" role="tabpanel" aria-labelledby="pills-profile-tab">
					<div class="row">
						<div class="col-md-4 pt-3">
							<div class="logo"></div>
						</div>
						<div class="col-md-8 pt-3 desc-text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="team" class="acc-color">
		<div class="container px-5 text-center pt-5">
			<h5 class="small-text">Lets Meet</h5>
			<h1 class="display-4 white-text text-center">Our Team</h1>
			<div class="row pb-5">
				<div class="col-md-4 my-4 px-5">
					<div class="img-hover-zoom img-hover-zoom--colorize">
						<img src="https://picsum.photos/800/800?image=342" width="100%">
						<div class="caption-name">John Doe</div>
						<div class="caption-desc">Head Manager</div>
					</div>
				</div>
				<div class="col-md-4 my-4 px-5">
					<div class="img-hover-zoom img-hover-zoom--colorize">
						<img src="https://picsum.photos/800/800?image=342" width="100%">
						<div class="caption-name">Susan Lee</div>
						<div class="caption-desc">Manager</div>
					</div>
				</div>
				<div class="col-md-4 my-4 px-5">
					<div class="img-hover-zoom img-hover-zoom--colorize">
						<img src="https://picsum.photos/800/800?image=342" width="100%">
						<div class="caption-name">Harold Dave</div>
						<div class="caption-desc">Supervisor</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
@endsection