<nav id="navbar-shadow" class="navbar navbar-expand-sm navbar-dark py-md-4 sticky-top prim-color">
	<div class="container">
		<a class="navbar-brand acc-color-text" href="index.php"><b>LexLite</b></a>
		<button id="custom-toggler" class="navbar-toggler"
		type="button"
		data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent"
		aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item px-md-4">
					<a class="nav-link" href="#">Edu Section</a>
				</li>
				<li class="nav-item px-md-4">
					{{--  <a class="nav-link" href="#">Contribute</a>  --}}
				</li>
				<li class="nav-item px-md-4">
					{{--  <a class="nav-link" href="#">Request Content</a>  --}}
				</li>
				<li class="nav-item px-md-4">
					<a class="nav-link" href="about-us.php">About Us</a>
				</li>
				<li class="nav-item px-md-4">
					<a class="nav-link" href="#">Support Us</a>
				</li>
				<!-- <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Dropdown
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="#">Action</a>
										<a class="dropdown-item" href="#">Another action</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Something else here</a>
							</div>
				</li> -->
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<div class="d-sm-none">
					<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</div>
				<div id="search-icon" class="d-none d-sm-block">
					<input type="search" placeholder="Search">
				</div>
			</form>
		</div>
	</div>
</nav>
