@extends('partials.layout')

@section('content')

<div class="container-fluid prim-color py-5">
    <div class="container">
        <div class="row py-4">
            <div class="col-md-8 text-white">
                {{-- <img src="{{ asset('img/6.jpg') }}" alt="Post photo" class="img-fluid my-3"> --}}
				<div class="plyr__video-embed" id="player" style="">
					{!! $subarticle->content !!}
				</div>
            </div>
            <div class="col-md-4 mt-3">
                {{--  <b class="text-uppercase grey-text text-lighten-5">Category</b>  --}}
                <h1 class="display-5 mt-1 acc-color-text">{{$subarticle->title}}</h1>
				<small class="grey-text text-lighten-4"><i class="fas fa-calendar-alt"></i> <b>{{ \Carbon\Carbon::parse($subarticle->created_at)->format('d F Y') }} </b> </small>

                <p><!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_inline_share_toolbox"></div>
                     </p>
            </div>
        </div>
        <div id="disqus_thread"></div>
    </div>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-6035751d80e54e63"></script>
<script>
    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://lexliteid.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
</script>
@endsection
