@extends('partials.header')
@extends('partials.nav')
<div class="container mt-5 pt-5">
            <div class="grid mt-5 pt-5 pb-5">
                    <div class="row">
                        <div class="col">
                           @foreach ($scheduleregists->slice(0,1) as $scheduleregist)
                           <figure class="effect-ravi">
                                <img src="{{$scheduleregist->image}}" style="width:2000px;height:300px;object-fit: cover" alt="img25" />
                                <figcaption>
                                    <h2> DOWNLOAD FORM REGISTRATION HERE}</h2>
                                    <p>
                                        <a href="regist/{{ $scheduleregist->id }}" download><i class="fa fa-download"></i></a>
    
                                    </p>
                                </figcaption>
                            </figure>
    
                           @endforeach
                        </div>
                    </div>
    
    
                    <div class="row">
                        @foreach ($schedules as $schedule)
                        <div class="col-md-4">
                                <figure class="effect-ravi">
                                    <img src="{{ $schedule->image }}" style="object-fit: cover;" alt="img17" />
                                    <figcaption>
                                        <h2>Event <span> {{ $schedule->year }}</span></h2>
                                        <p>
                                            <a href="/schedule/detail/{{$schedule->id }}"><i class="fa fa-search"></i></a>
                                        </p>
                                    </figcaption>
                                </figure>
                            </div>                                
                        @endforeach
                    </div>
    
                </div>
        
    </div>
@extends('partials.footer')