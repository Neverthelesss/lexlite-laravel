@extends('layouts.admin')
@section('content')
@can('media_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('warning'))
                <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
                </div>
            @endif
            {{--  <a class="btn btn-xs btn-success" href="{{ route('admin.subarticles.creates', $media->id) }}">
                Add New Data
            </a>  --}}
            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModal">
                Add New Data
              </button>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('global.article.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>Id Post</th>
                        <th>
                            {{ trans('global.article.fields.title') }}
                        </th>
                        <th>
                            {{ trans('global.article.fields.image') }}
                        </th>
                        <th>
                            Time Created
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Subarticles as $key => $Subarticle)
                        <tr data-entry-id="{{ $Subarticle->id }}">
                            <td>

                            </td>
                            <td>
                                {{  $Subarticle->id ?? '' }}
                            </td>
                            <td>
                                {{ $Subarticle->title ?? '' }}
                            </td>
                            <td>
                               <img height="200" src=" {{ $Subarticle->image ?? '' }}" alt="">
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($Subarticle->created_at)->diffForHumans() }}
                            </td>
                            <td>
                                @can('media_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.subarticles.shows', $Subarticle->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('media_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.subarticles.edit', $Subarticle->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('media_delete')
                                <form action="{{ route('admin.subarticles.destroy', $Subarticle->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{{--  modal list 1  --}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body ">
                {{--  isi  --}}
                <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
                    <div class="card">
                        <div class="card-header">
                            {{ trans('global.create') }} {{ trans('global.article.title_singular') }}
                        </div>

                        <div class="card-body">
                            <form action="{{ route("admin.subarticles.store") }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                    <input type="hidden" name="article_id" value="{{ $media->id }}" readonly>
                                    <label for="name">{{ trans('global.article.fields.title') }}*</label>
                                    <input type="text" id="name" name="title" class="form-control" value="{{ old('title', isset($article) ? $article->title : '') }}">
                                    @if($errors->has('title'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('title') }}
                                        </em>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                        </span>
                                        <input id="thumbnail" class="form-control" type="text" name="image">
                                    </div>
                                    <img id="holder" style="margin-top:15px;max-height:100px;">
                                    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
                                    <script>
                                    var route_prefix = "http://localhost:8000/laravel-filemanager";
                    $('#lfm').filemanager('image', {prefix: route_prefix});
                                    </script>
                                    </div>

                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                        <label for="name">{{ trans('global.article.fields.title') }}*</label>
                                        <input type="text" id="content" name="content" class="form-control" value="{{ old('title', isset($article) ? $article->content : '') }}">
                                        @if($errors->has('content'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('content') }}
                                            </em>
                                        @endif
                                    </div>


                                <div>
                                    <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                                </div>

                        </div>
                    </div>
                {{--  end of isi  --}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
        </div>
    </form>
      </div>
    </div>
  </div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.subarticles.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('media_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
