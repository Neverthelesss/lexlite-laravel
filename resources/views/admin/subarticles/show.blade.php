@extends('layouts.admin')
@section('content')

<a class="btn btn-primary btn-lg mb-4" href="{{ url()->previous() }}"><i class="fa fa-arrow-left
    "></i> Back to article list</a>
    <br>
<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>
    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.article.fields.title') }}
                    </th>
                    <td>
                        {{ $subarticle->title }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.article.fields.image') }} thumbnail
                    </th>
                    <td>
                        <img src="{{ $subarticle->image }}" alt="">
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.article.fields.content') }}
                    </th>
                    <td>
                        {!! $subarticle->content !!}
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

@endsection
