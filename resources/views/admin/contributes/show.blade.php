@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        Nama
                    </th>
                    <td>
                        {{ $contribute->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        link
                    </th>
                    <td>
                       {{ $contribute->email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        link
                    </th>
                    <td>
                       <a herf="{{ $contribute->link }}" target="blank">{{ $contribute->link }}</a>
                    </td>
                </tr>


            </tbody>
        </table>
    </div>
</div>

@endsection
