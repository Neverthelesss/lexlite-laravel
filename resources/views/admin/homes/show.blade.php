@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.home.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.home.fields.about') }}
                    </th>
                    <td>
                        {!! $home->about !!}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.home.fields.video') }}
                    </th>
                    <td>
                            <iframe class="w-100" height="248" src="{{ $home->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.home.fields.video_text') }}
                    </th>
                    <td>
                        {{ $home->video_text }}
                    </td>
                </tr>
              
            </tbody>
        </table>
    </div>
</div>

@endsection