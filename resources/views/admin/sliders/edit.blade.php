@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.user.title_singular') }}
    </div>

    <div class="card-body">
      <form action="{{ route("admin.sliders.update", [$slider->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')   
         <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">{{ trans('global.slider.fields.name') }}*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($slider) ? $slider->name : '') }}">
                @if($errors->has('name'))
                    <em class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </em>
                @endif
            </div>
            
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                      <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                        <i class="fa fa-picture-o"></i> Choose
                      </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="image" value="{{ old('image', isset($slider) ? $slider->image : '') }}">
                  </div>
                  <img id="holder" style="margin-top:15px;max-height:100px;">
                  <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
                  <script>
                  var route_prefix = "http://127.0.0.1:8000/laravel-filemanager";
 $('#lfm').filemanager('image', {prefix: route_prefix});
                </script>
                </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection