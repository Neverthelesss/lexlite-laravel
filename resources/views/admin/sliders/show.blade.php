@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.slider.fields.name') }}
                    </th>
                    <td>
                        {{ $slider->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.slider.fields.image') }}
                    </th>
                    <td>
                       <img src=" {{ $slider->image }}" alt="">
                    </td>
                </tr>
                
            
            </tbody>
        </table>
    </div>
</div>

@endsection