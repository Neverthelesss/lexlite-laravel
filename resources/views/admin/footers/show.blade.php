@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.user.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
               
                <tr>
                    <th>
                        {{ trans('global.footer.fields.content') }}
                    </th>
                    <td>
                        {!! $footer->content !!}
                    </td>
                </tr>
            
            </tbody>
        </table>
    </div>
</div>

@endsection