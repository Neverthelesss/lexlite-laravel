@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.user.title_singular') }}
    </div>

    <div class="card-body">
      <form action="{{ route("admin.footers.update", [$footer->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                <label for="name">{{ trans('global.footer.fields.content') }}*</label>
                <textarea name="content" class="form-control my-editor">{{ old('content', isset($footer) ? $footer->content : '') }}</textarea>
                <script>
                  var editor_config = {
                    path_absolute : "/",
                    selector: "textarea.my-editor",
                    plugins: [
                      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                      "searchreplace wordcount visualblocks visualchars code fullscreen",
                      "insertdatetime media nonbreaking save table contextmenu directionality",
                      "emoticons template paste textcolor colorpicker textpattern"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                    relative_urls: false,
                    file_browser_callback : function(field_name, url, type, win) {
                      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
                
                      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                      if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                      } else {
                        cmsURL = cmsURL + "&type=Files";
                      }
                
                      tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                      });
                    }
                  };
                
                  tinymce.init(editor_config);
                </script>
            </div>
                
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection