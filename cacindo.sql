-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Agu 2020 pada 10.23
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cacindo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `articles`
--

INSERT INTO `articles` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Testing', 'testing', '<div>\r\n<div>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur,&nbsp;adipisicing&nbsp;elit.&nbsp;Repudiandae&nbsp;voluptatum&nbsp;doloribus&nbsp;culpa&nbsp;excepturi&nbsp;voluptates&nbsp;rem&nbsp;itaque&nbsp;harum,&nbsp;iusto&nbsp;suscipit&nbsp;provident&nbsp;fugit&nbsp;nulla&nbsp;modi&nbsp;inventore,&nbsp;magni&nbsp;nisi&nbsp;neque&nbsp;doloremque&nbsp;dolore&nbsp;delectus!</div>\r\n<div>\r\n<div>\r\n<div>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur,&nbsp;adipisicing&nbsp;elit.&nbsp;Repudiandae&nbsp;voluptatum&nbsp;doloribus&nbsp;culpa&nbsp;excepturi&nbsp;voluptates&nbsp;rem&nbsp;itaque&nbsp;harum,&nbsp;iusto&nbsp;suscipit&nbsp;provident&nbsp;fugit&nbsp;nulla&nbsp;modi&nbsp;inventore,&nbsp;magni&nbsp;nisi&nbsp;neque&nbsp;doloremque&nbsp;dolore&nbsp;delectus!</div>\r\n<div>\r\n<div>\r\n<div>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur,&nbsp;adipisicing&nbsp;elit.&nbsp;Repudiandae&nbsp;voluptatum&nbsp;doloribus&nbsp;culpa&nbsp;excepturi&nbsp;voluptates&nbsp;rem&nbsp;itaque&nbsp;harum,&nbsp;iusto&nbsp;suscipit&nbsp;provident&nbsp;fugit&nbsp;nulla&nbsp;modi&nbsp;inventore,&nbsp;magni&nbsp;nisi&nbsp;neque&nbsp;doloremque&nbsp;dolore&nbsp;delectus!</div>\r\n<div>\r\n<div>\r\n<div>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur,&nbsp;adipisicing&nbsp;elit.&nbsp;Repudiandae&nbsp;voluptatum&nbsp;doloribus&nbsp;culpa&nbsp;excepturi&nbsp;voluptates&nbsp;rem&nbsp;itaque&nbsp;harum,&nbsp;iusto&nbsp;suscipit&nbsp;provident&nbsp;fugit&nbsp;nulla&nbsp;modi&nbsp;inventore,&nbsp;magni&nbsp;nisi&nbsp;neque&nbsp;doloremque&nbsp;dolore&nbsp;delectus!</div>\r\n<div>\r\n<div>\r\n<div>Lorem&nbsp;ipsum&nbsp;dolor&nbsp;sit&nbsp;amet&nbsp;consectetur,&nbsp;adipisicing&nbsp;elit.&nbsp;Repudiandae&nbsp;voluptatum&nbsp;doloribus&nbsp;culpa&nbsp;excepturi&nbsp;voluptates&nbsp;rem&nbsp;itaque&nbsp;harum,&nbsp;iusto&nbsp;suscipit&nbsp;provident&nbsp;fugit&nbsp;nulla&nbsp;modi&nbsp;inventore,&nbsp;magni&nbsp;nisi&nbsp;neque&nbsp;doloremque&nbsp;dolore&nbsp;delectus!</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '2020-08-04 22:21:09', '2020-08-04 22:21:09', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `events`
--

INSERT INTO `events` (`id`, `title`, `image`, `content`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Event 1', 'img/1.jpg', '<p>dasnidasnidasnda</p>', 'training', '2020-08-05 00:49:43', '2020-08-05 01:15:35', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `footers`
--

CREATE TABLE `footers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `founders`
--

CREATE TABLE `founders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `homes`
--

CREATE TABLE `homes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `medias`
--

CREATE TABLE `medias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(26, '2014_10_12_100000_create_password_resets_table', 1),
(27, '2019_04_15_191331679173_create_1555355612601_permissions_table', 1),
(28, '2019_04_15_191331731390_create_1555355612581_roles_table', 1),
(29, '2019_04_15_191331779537_create_1555355612782_users_table', 1),
(30, '2019_04_15_191332603432_create_1555355612603_permission_role_pivot_table', 1),
(31, '2019_04_15_191332791021_create_1555355612790_role_user_pivot_table', 1),
(32, '2019_04_15_191441675085_create_1555355681975_products_table', 1),
(33, '2019_08_19_000000_create_failed_jobs_table', 1),
(34, '2020_04_25_061819_create_articles_table', 1),
(35, '2020_04_25_061846_create_events_table', 1),
(36, '2020_04_25_061918_create_footers_table', 1),
(37, '2020_04_25_061934_create_founders_table', 1),
(38, '2020_04_25_061951_create_homes_table', 1),
(39, '2020_04_25_062010_create_news_table', 1),
(40, '2020_04_25_062020_create_pages_table', 1),
(41, '2020_04_25_062026_create_partners_table', 1),
(42, '2020_04_25_062036_create_programs_table', 1),
(43, '2020_04_25_062045_create_reports_table', 1),
(44, '2020_04_25_062103_create_researchs_table', 1),
(45, '2020_04_25_062113_create_schedules_table', 1),
(46, '2020_04_25_062123_create_scheduleregists_table', 1),
(47, '2020_04_25_062153_create_subscribes_table', 1),
(48, '2020_04_27_091116_create_sliders_table', 1),
(49, '2020_08_05_023829_create_medias_table', 1),
(50, '2020_08_05_023847_create_resources_table', 1),
(51, '2020_08_05_080156_create_media_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ABOUT CAC INDO', 'about', '<div>\r\n<div style=\"text-align: center;\">CAC&nbsp;menyediakan&nbsp;platform&nbsp;bagi&nbsp;sektor&nbsp;bisnis&nbsp;untuk&nbsp;menjadi&nbsp;bagian&nbsp;dari&nbsp;solusi</div>\r\n<div style=\"text-align: center;\">untuk&nbsp;masalah&nbsp;korupsi.&nbsp;Melibatkan&nbsp;lebih&nbsp;banyak&nbsp;entitas&nbsp;bisnis&nbsp;dan</div>\r\n<div style=\"text-align: center;\">membantu&nbsp;mereka&nbsp;memasang&nbsp;kebijakan&nbsp;dan&nbsp;mekanisme&nbsp;anti-korupsi&nbsp;akan</div>\r\n<div style=\"text-align: center;\">secara&nbsp;langsung&nbsp;mengurangi&nbsp;risiko&nbsp;penyuapan&nbsp;secara&nbsp;keseluruhan.&nbsp;</div>\r\n<div style=\"text-align: center;\">Mengingat&nbsp;momentum&nbsp;anti-korupsi&nbsp;yang&nbsp;terus&nbsp;bergulir&nbsp;dan&nbsp;partisipasi</div>\r\n<div style=\"text-align: center;\">perusahaan&nbsp;swasta&nbsp;yang&nbsp;semakin&nbsp;meningkat&nbsp;dari&nbsp;semua&nbsp;jenis&nbsp;dan&nbsp;ukuran,</div>\r\n<div style=\"text-align: center;\">CAC&nbsp;berharap&nbsp;untuk&nbsp;mencapai&nbsp;massa&nbsp;kritis&nbsp;dan&nbsp;menciptakan&nbsp;tekanan&nbsp;teman</div>\r\n<div style=\"text-align: center;\">sebaya&nbsp;bagi&nbsp;para&nbsp;pemain&nbsp;yang&nbsp;tersisa&nbsp;untuk&nbsp;juga&nbsp;meningkatkan&nbsp;standar</div>\r\n<div style=\"text-align: center;\">kepatuhan&nbsp;mereka&nbsp;untuk&nbsp;memenuhi&nbsp;standar&nbsp;anggota&nbsp;CAC.&nbsp;Ketika&nbsp;mayoritas</div>\r\n<div style=\"text-align: center;\">pemain&nbsp;utama&nbsp;dari&nbsp;masing-masing&nbsp;industri&nbsp;mengadopsi&nbsp;praktik&nbsp;bisnis&nbsp;bersih,</div>\r\n<div style=\"text-align: center;\">itu&nbsp;akan&nbsp;secara&nbsp;signifikan&nbsp;mengubah&nbsp;lanskap&nbsp;bisnis&nbsp;keseluruhan&nbsp;dan&nbsp;korupsi</div>\r\n<div style=\"text-align: center;\">dalam&nbsp;segala&nbsp;bentuk&nbsp;pada&nbsp;akhirnya&nbsp;akan&nbsp;menjadi&nbsp;praktik&nbsp;yang&nbsp;tidak&nbsp;dapat</div>\r\n<div style=\"text-align: center;\">diterima.&nbsp;Dengan&nbsp;jaringan&nbsp;bisnis&nbsp;bersih&nbsp;yang&nbsp;luas,&nbsp;CAC&nbsp;juga&nbsp;berniat&nbsp;untuk</div>\r\n<div style=\"text-align: center;\">memanfaatkan&nbsp;pengetahuan&nbsp;yang&nbsp;dikumpulkan&nbsp;dari&nbsp;sektor&nbsp;swasta&nbsp;untuk</div>\r\n<div style=\"text-align: center;\">membantu&nbsp;pemerintah&nbsp;mengidentifikasi&nbsp;titik&nbsp;problema&nbsp;dalam&nbsp;proses</div>\r\n<div style=\"text-align: center;\">pelayanan&nbsp;publik&nbsp;yang&nbsp;rentan&nbsp;terhadap&nbsp;risiko&nbsp;korupsi&nbsp;dan&nbsp;membantu&nbsp;mereka</div>\r\n<div style=\"text-align: center;\">mengembangkan&nbsp;solusi&nbsp;potensial&nbsp;untuk&nbsp;mengurangi&nbsp;risiko&nbsp;tersebut.&nbsp;&nbsp;&nbsp;&nbsp;</div>\r\n</div>', '2020-08-05 00:52:32', '2020-08-05 00:52:46', NULL),
(2, 'Member CAC INDO', 'member', '<table style=\"border-collapse: collapse; width: 100%; margin-left: auto; margin-right: auto; height: 51px;\" border=\"1\">\r\n<tbody>\r\n<tr style=\"height: 17px;\">\r\n<td style=\"width: 33.3333%; text-align: center; height: 17px;\">NO</td>\r\n<td style=\"width: 33.3333%; text-align: center; height: 17px;\">Name</td>\r\n<td style=\"width: 33.3333%; height: 17px; text-align: center;\">Tittle</td>\r\n</tr>\r\n<tr style=\"height: 17px;\">\r\n<td style=\"width: 33.3333%; height: 17px; text-align: center;\">1</td>\r\n<td style=\"width: 33.3333%; height: 17px; text-align: center;\">lorem</td>\r\n<td style=\"width: 33.3333%; height: 17px; text-align: center;\">dolor</td>\r\n</tr>\r\n<tr style=\"height: 17px;\">\r\n<td style=\"width: 33.3333%; height: 17px; text-align: center;\">2</td>\r\n<td style=\"width: 33.3333%; height: 17px; text-align: center;\">ipsum</td>\r\n<td style=\"width: 33.3333%; height: 17px; text-align: center;\">amet</td>\r\n</tr>\r\n</tbody>\r\n</table>', '2020-08-05 00:54:06', '2020-08-05 00:54:06', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `partners`
--

CREATE TABLE `partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(2, 'permission_create', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(3, 'permission_edit', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(4, 'permission_show', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(5, 'permission_delete', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(6, 'permission_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(7, 'role_create', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(8, 'role_edit', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(9, 'role_show', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(10, 'role_delete', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(11, 'role_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(12, 'user_create', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(13, 'user_edit', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(14, 'user_show', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(15, 'user_delete', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(16, 'user_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(17, 'product_create', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(18, 'product_edit', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(19, 'product_show', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(20, 'product_delete', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(21, 'product_access', '2019-04-15 12:14:42', '2019-04-15 12:14:42', NULL),
(22, 'event_access', '2020-08-04 22:10:32', '2020-08-04 22:10:32', NULL),
(23, 'event_create', '2020-08-04 22:10:37', '2020-08-04 22:10:37', NULL),
(24, 'event_delete', '2020-08-04 22:10:44', '2020-08-04 22:10:44', NULL),
(25, 'event_edit', '2020-08-04 22:10:48', '2020-08-04 22:10:48', NULL),
(26, 'event_show', '2020-08-04 22:10:53', '2020-08-04 22:10:53', NULL),
(27, 'resource_access', '2020-08-04 22:11:07', '2020-08-04 22:11:07', NULL),
(28, 'resource_create', '2020-08-04 22:11:13', '2020-08-04 22:11:13', NULL),
(29, 'resource_delete', '2020-08-04 22:11:17', '2020-08-04 22:11:17', NULL),
(30, 'resource_edit', '2020-08-04 22:11:22', '2020-08-04 22:11:22', NULL),
(31, 'resource_show', '2020-08-04 22:11:28', '2020-08-04 22:11:28', NULL),
(32, 'slider_access', '2020-08-04 22:11:41', '2020-08-04 22:11:41', NULL),
(33, 'slider_create', '2020-08-04 22:11:46', '2020-08-04 22:11:46', NULL),
(34, 'slider_delete', '2020-08-04 22:11:50', '2020-08-04 22:11:50', NULL),
(35, 'slider_edit', '2020-08-04 22:11:55', '2020-08-04 22:11:55', NULL),
(36, 'slider_show', '2020-08-04 22:11:59', '2020-08-04 22:11:59', NULL),
(37, 'media_access', '2020-08-04 22:12:09', '2020-08-04 22:12:09', NULL),
(38, 'media_create', '2020-08-04 22:12:13', '2020-08-04 22:12:13', NULL),
(39, 'media_delete', '2020-08-04 22:12:21', '2020-08-04 22:12:21', NULL),
(40, 'media_edit', '2020-08-04 22:12:27', '2020-08-04 22:12:27', NULL),
(41, 'media_show', '2020-08-04 22:12:33', '2020-08-04 22:12:33', NULL),
(42, 'partner_access', '2020-08-04 22:13:08', '2020-08-04 22:13:08', NULL),
(43, 'partner_create', '2020-08-04 22:13:13', '2020-08-04 22:13:13', NULL),
(44, 'partner_delete', '2020-08-04 22:13:18', '2020-08-04 22:13:18', NULL),
(45, 'partner_edit', '2020-08-04 22:13:23', '2020-08-04 22:13:23', NULL),
(46, 'partner_show', '2020-08-04 22:13:29', '2020-08-04 22:13:29', NULL),
(47, 'page_access', '2020-08-04 22:13:46', '2020-08-04 22:13:46', NULL),
(48, 'page_create', '2020-08-05 00:50:39', '2020-08-05 00:50:39', NULL),
(49, 'page_delete', '2020-08-05 00:50:43', '2020-08-05 00:50:43', NULL),
(50, 'page_edit', '2020-08-05 00:50:49', '2020-08-05 00:50:49', NULL),
(51, 'page_show', '2020-08-05 00:50:59', '2020-08-05 00:50:59', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51);

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `programs`
--

CREATE TABLE `programs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `researchs`
--

CREATE TABLE `researchs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `resources`
--

CREATE TABLE `resources` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `resources`
--

INSERT INTO `resources` (`id`, `title`, `image`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Testing', 'img/1.jpg', '<p>ini hanyalah text&nbsp;<strong> dengan bold&nbsp;</strong>dan dengan&nbsp;<em>Italic</em></p>', '2020-08-05 00:45:23', '2020-08-05 00:45:23', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', '2019-04-15 12:13:32', '2019-04-15 12:13:32', NULL),
(2, 'User', '2019-04-15 12:13:32', '2019-04-15 12:13:32', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `scheduleregists`
--

CREATE TABLE `scheduleregists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `schedules`
--

CREATE TABLE `schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `subscribes`
--

CREATE TABLE `subscribes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$imU.Hdz7VauIT3LIMCMbsOXvaaTQg6luVqkhfkBcsUd.SJW2XSRKO', NULL, '2019-04-15 12:13:32', '2019-04-15 12:13:32', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `footers`
--
ALTER TABLE `footers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `founders`
--
ALTER TABLE `founders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `permission_role_role_id_foreign` (`role_id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `researchs`
--
ALTER TABLE `researchs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `scheduleregists`
--
ALTER TABLE `scheduleregists`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `subscribes`
--
ALTER TABLE `subscribes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `footers`
--
ALTER TABLE `footers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `founders`
--
ALTER TABLE `founders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `homes`
--
ALTER TABLE `homes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `medias`
--
ALTER TABLE `medias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT untuk tabel `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `programs`
--
ALTER TABLE `programs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `researchs`
--
ALTER TABLE `researchs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `resources`
--
ALTER TABLE `resources`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `scheduleregists`
--
ALTER TABLE `scheduleregists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `subscribes`
--
ALTER TABLE `subscribes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ketidakleluasaan untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
